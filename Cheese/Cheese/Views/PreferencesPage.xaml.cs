﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Controls;
using System.Net;
using System.IO;
using System.ComponentModel;
using Cheese.ViewModels;
using DocumentFormat.OpenXml.Vml.Office;

namespace Cheese.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PreferencesPage : ContentPage
    {
        public PreferencesViewModel PreferencesViewModel { get; set; }
        public PreferencesPage()
        {
            InitializeComponent();

            PreferencesViewModel = LoadModel(); // ToDo: Personen Namen werden nicht gespeichert bzw. geladen

            BindingContext = this;

            PreferencesViewModel.PropertyChanged += SaveModelOnChanged;

            DropboxUtilities.AccessTokenChanged += OnAccessTokenChangedEvent;

            OnAccessTokenChangedEvent(new object(),new PropertyChangedEventArgs(""));
        }

        private PreferencesViewModel LoadModel()
        {
            if (!Application.Current.Properties.ContainsKey(nameof(PreferencesViewModel)))
                return new PreferencesViewModel();
            else
                return PreferencesViewModel.Deserialize((string)Application.Current.Properties[nameof(PreferencesViewModel)]);               
        }

        private void SaveModelOnChanged(object sender, PropertyChangedEventArgs e)
        {
            Application.Current.Properties[nameof(PreferencesViewModel)] = PreferencesViewModel.Serialize();
        }

        private async void btnLogin_Clicked(object sender, EventArgs e)
        {
            btnLogin.Clicked -= btnLogin_Clicked;

            if (DropboxUtilities.AccessToken == null)
                await DropboxUtilities.Authorize();

            btnLogin.Clicked += btnLogin_Clicked;
        }

        private void btnLogout_Clicked(object sender, EventArgs e)
        {
            LoggedOutPageAppearance();

            DropboxUtilities.AccessToken = null;
            DropboxUtilities.DropboxClient = null;
        }

        private async void OnAccessTokenChangedEvent(object sender, PropertyChangedEventArgs e)
        {
            if (DropboxUtilities.AccessToken != null)
                await LoggedInPageAppearance();
            else
                LoggedOutPageAppearance();
        }

        private async Task LoggedInPageAppearance()
        {
            if (DropboxUtilities.AccessToken != null)
            {
                btnLogout.IsVisible = true;
                btnLogin.IsVisible = false;
                var taskAccount = await DropboxUtilities.DropboxClient.Users.GetCurrentAccountAsync();

                if (!String.IsNullOrEmpty(taskAccount.ProfilePhotoUrl))
                    PreferencesViewModel.ImageSource = (taskAccount.ProfilePhotoUrl);
                else
                    PreferencesViewModel.ImageSource = "icon_round";

                PreferencesViewModel.User = taskAccount.Email;
                PreferencesViewModel.AccountType = "Dropbox Account";
            }

        }
        private void LoggedOutPageAppearance()
        {
            btnLogout.IsVisible = false;
            btnLogin.IsVisible = true;

            PreferencesViewModel.ImageSource = "icon_round";
            PreferencesViewModel.User = "User";
            PreferencesViewModel.AccountType = "Account Type";
        }
    }
}