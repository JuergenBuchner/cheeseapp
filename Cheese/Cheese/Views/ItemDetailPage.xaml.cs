﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Cheese.Models;
using Cheese.ViewModels;

namespace Cheese.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class ItemDetailPage : ContentPage
    {
        ItemDetailViewModel viewModel;

        public ItemDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;

            if (Application.Current.Properties.ContainsKey(nameof(PreferencesViewModel)))
            {
                PreferencesViewModel preferences = PreferencesViewModel.Deserialize((string)Application.Current.Properties[nameof(PreferencesViewModel)]);

                if(!String.IsNullOrWhiteSpace(preferences.Person1))
                    ratingPerson1Name.Text = "Bewertung: " + preferences.Person1;

                if(!String.IsNullOrWhiteSpace(preferences.Person2))
                    ratingPerson2Name.Text = "Bewertung: " + preferences.Person2;
            }
        }

        public ItemDetailPage()
        {
            InitializeComponent();

            var item = new CheeseRating
            {
                Name = "Item 1",
                Beschreibung = "This is an item description."
            };

            viewModel = new ItemDetailViewModel(item);
            BindingContext = viewModel;
        }
    }
}