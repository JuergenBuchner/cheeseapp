﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Cheese.Models;
using Cheese.Views;
using Cheese.ViewModels;
using Controls;
using System.Collections.Specialized;

namespace Cheese.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class ItemsPage : ContentPage
    {
        ItemsViewModel viewModel;
        PreferencesViewModel Preferences;
        public ItemsPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new ItemsViewModel();

            #region Filter and Sort
            AddPersonsToRatingPicker();
            pickerRating.SelectedIndex = 0;

            pickerSort.SelectedIndex = 0;

            viewModel.AllItems.CollectionChanged += Items_CollectionChanged;
            #endregion

            UploadHandler.Instance.PropertyChanged += ActiveJobChanged;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.AllItems.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as CheeseRating;
            if (item == null)
                return;

            await Navigation.PushAsync(new ItemDetailPage(new ItemDetailViewModel(item)));

            // Manually deselect item.
            ItemsListView.SelectedItem = null;
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            btnAdd.Clicked -= AddItem_Clicked;

            if (DropboxUtilities.AccessToken == null)
            {
                await DisplayAlert("Account", "Damit sie eine Bewertung abgeben können, müssen sie sich vorher in den Einstellungen einloggen.", "Ok");

                //await Navigation.PopModalAsync(); // zurück zum Home-Screen
            }
            else
            {
                await Navigation.PushModalAsync(new NavigationPage(new NewItemPage()));
            }
            btnAdd.Clicked += AddItem_Clicked;
        }


        #region Upload Status Display
        private void ActiveJobChanged(object sender, PropertyChangedEventArgs e)
        {
            if(UploadHandler.Instance.IsBusy)
                iconUploads.IconImageSource = "ic_backup_white_48dp";
            else
                iconUploads.IconImageSource = "ic_cloud_done_white_48dp";

        }

        private async void iconUploads_Clicked(object sender, EventArgs e)
        {
            string displayText = string.Empty;

            if(UploadHandler.Instance.ActiveJobs.Count > 0)
                displayText = String.Format("Es wird gerade die Bewertung vom Käse '{0}' hochgeladen.", UploadHandler.Instance.ActiveJobs.First().JobTitle);

            if(UploadHandler.Instance.JobQueue.Count > 0)
            {
                if (!String.IsNullOrWhiteSpace(displayText))
                    displayText += Environment.NewLine + Environment.NewLine;
                displayText += String.Format("Es sind noch folgende Uploads ausstehend: {0}", String.Join(", ",UploadHandler.Instance.JobQueue));
            }

            if (string.IsNullOrWhiteSpace(displayText))
                displayText = String.Format("Alle Bewertungen wurden hochgeladen");

            await DisplayAlert("Upload Info",displayText,"Ok");
        }
        #endregion

        #region Filter and Sort
        private void AddPersonsToRatingPicker()
        {
            pickerRating.Items.Add("Durchschnitt");

            if (Application.Current.Properties.ContainsKey(nameof(PreferencesViewModel)))
            {
                Preferences = PreferencesViewModel.Deserialize((string)Application.Current.Properties[nameof(PreferencesViewModel)]);
                if (!string.IsNullOrWhiteSpace(Preferences.Person1))
                    pickerRating.Items.Add(Preferences.Person1);
                else
                    pickerRating.Items.Add("Person1");

                if (!string.IsNullOrWhiteSpace(Preferences.Person2))
                    pickerRating.Items.Add(Preferences.Person2);
                else
                    pickerRating.Items.Add("Person2");
            }
            else
            {
                pickerRating.Items.Add("Person1");
                pickerRating.Items.Add("Person2");
            }
        }
        
        private Dictionary<string, bool> sortingDirectionAscending = new Dictionary<string, bool>();

        private void FilterAndOrderListByPickerAndSwitch()
        {
            if(viewModel != null && viewModel.AllItems != null & viewModel.AllItems.Count > 0)  // Count > 0 ist notwendig dass er erst nach erstmaligem laden sortiert... ist noch unsauber gelöst
            {
                List<CheeseRating> items = new List<CheeseRating>();

                foreach (CheeseRating rating in viewModel.AllItems)
                    items.Add(CheeseRating.ParseFromRawData(rating.GetData())); // Damit Daten bei Bewertungsanzeige übernommen wird

                if (pickerRating.SelectedIndex != -1)
                {
                    foreach (CheeseRating rating in items)
                    {
                        string selectedRating = (string)pickerRating.SelectedItem;
                        
                        if(selectedRating == "Durchschnitt")
                            rating.Bewertung = rating.BewertungDurchschnitt;
                        else if(selectedRating == "Person1" || (Preferences != null && selectedRating == Preferences.Person1))
                            rating.Bewertung = rating.BewertungPerson1;
                        else if (selectedRating == "Person2" || (Preferences != null && selectedRating == Preferences.Person2))
                            rating.Bewertung = rating.BewertungPerson2;
                    }
                }                

                if(pickerFilter.SelectedIndex != -1)
                {
                    string selectedItem = pickerFilter.Items[pickerFilter.SelectedIndex].ToString();
                    items = items.Where(rating => rating.GetType().GetProperty(selectedItem).GetValue(rating, null).ToString().ToLower().Contains((entryFilter.Text ?? string.Empty).ToLower())).ToList();
                }

                if (pickerSort.SelectedIndex != -1)
                {
                    string selectedItem = pickerSort.Items[pickerSort.SelectedIndex].ToString();
                    if (switchDescending.IsToggled)
                        items = items.OrderByDescending(rating => rating.GetType().GetProperty(selectedItem).GetValue(rating, null)).ToList(); // Sollte über das ViewModel gelöst werden
                    else
                        items = items.OrderBy(rating => rating.GetType().GetProperty(selectedItem).GetValue(rating, null)).ToList();
                }
                viewModel.FilteredAndSortedItems.Clear();

                foreach (CheeseRating rating in items)
                    viewModel.FilteredAndSortedItems.Add(rating);
            }
        }

        private void PickerSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterAndOrderListByPickerAndSwitch();
        }

        private async void switchDescending_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            FilterAndOrderListByPickerAndSwitch();
        }

        private void entryFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            FilterAndOrderListByPickerAndSwitch();
        }

        private void pickerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterAndOrderListByPickerAndSwitch();
        }

        internal void pickerRating_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterAndOrderListByPickerAndSwitch();
        }

        private void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            FilterAndOrderListByPickerAndSwitch(); // Anwendung bei jedem mal hinzufügen. Nicht besonders Resourcesparend
        }

        private void filterAndSortSwitch_Toggled(object sender, ToggledEventArgs e)
        {
            int currentSelectedIndex = pickerRating.SelectedIndex;
            pickerRating.Items.Clear();
            AddPersonsToRatingPicker();
            pickerRating.SelectedIndex = currentSelectedIndex;
        }

        #endregion Filter and Sort
    }
}