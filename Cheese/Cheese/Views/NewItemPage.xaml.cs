﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Controls;

using Cheese.Models;
using Wine.Utilities;
using Cheese.Controls;
using Cheese.ViewModels;

namespace Cheese.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class NewItemPage : ContentPage
    {
        public CheeseRating Item { get; set; }
        private string ratingHeaderTemplateNoName = "Bewertung: Person {0}";
        public NewItemPage()
        {
            InitializeComponent();

            if (Application.Current.Properties.ContainsKey(nameof(PreferencesViewModel)))
            {
                PreferencesViewModel preferences = PreferencesViewModel.Deserialize((string)Application.Current.Properties[nameof(PreferencesViewModel)]);
                if (!string.IsNullOrWhiteSpace(preferences.Person1))
                    lblPerson1.Text = "Bewertung: " + preferences.Person1;
                else
                    lblPerson1.Text = String.Format(ratingHeaderTemplateNoName, 1);

                if (!string.IsNullOrWhiteSpace(preferences.Person2))
                    lblPerson2.Text = "Bewertung: " + preferences.Person2;
                else
                    lblPerson2.Text = String.Format(ratingHeaderTemplateNoName, 2);
            }
            else
            {
                lblPerson1.Text = String.Format(ratingHeaderTemplateNoName,1);
                lblPerson2.Text = String.Format(ratingHeaderTemplateNoName,2);
            }


            Item = new CheeseRating
            {
            };

#if DEBUG
            var init = System.Threading.Tasks.Task.Run(() => InitialStartup.Initialize());
            init.Wait();
            Random random = new Random();
            Item.Barcode = "1234567890123";
            Item.Beschreibung = "Test";
            Item.BewertungPerson1 = random.Next(1, 10); ;
            Item.BewertungPerson2 = random.Next(1, 10); ;
            Item.Bewertung = Item.BewertungDurchschnitt;
            Item.Datum = DateTime.Now;
            Item.Erwerb = "erwerb";
            Item.Hersteller = "hersteller";
            Item.Name = "TestName " + DateTime.Now.ToLongTimeString();
            Item.Sorte = "Weichkäse";
            //XlsxUploadJob job = new XlsxUploadJob(Item);
            //UploadHandler.Instance.EnqueueJob(job);
#endif
            tBxSorte.ItemsSource = CheeseResource.CheeseTypes;

            BindingContext = this;

            sldBewertungPerson1.ValueChanged += OnSliderPerson1ValueChanged;
            sldBewertungPerson2.ValueChanged += OnSliderPerson2ValueChanged;
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            try
            { 
                UIUtilities.ThrowExceptionIfEntryIsNoInteger(tBxBarcode, "Barcode");
                if (tBxBarcode.Text.Length != 13)
                    throw new ArgumentException("Ein EAN Barcode muss 13 Ziffern enthalten");
                UIUtilities.ThrowExceptionIfEntryIsNullOrEmpty(tBxErwerb, "Erwerb");
                UIUtilities.ThrowExceptionIfEntryIsNullOrEmpty(tBxName, "Name");
                UIUtilities.ThrowExceptionIfEntryIsNullOrEmpty(tBxHersteller, "Hersteller");
                UIUtilities.ThrowExceptionIfPickerIsUnselected(tBxSorte, "Sorte");
            }
            catch (ArgumentException ex)
            {
                await DisplayAlert("Eingabe vergessen", ex.Message, "Ok");
                return;
            }
            Item.Bewertung = Item.BewertungDurchschnitt;
            MessagingCenter.Send(this, "Add", Item);
            XlsxUploadJob job = new XlsxUploadJob(Item);
            UploadHandler.Instance.EnqueueJob(job);
            await Navigation.PopModalAsync();
        }

        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        public void OnSliderPerson1ValueChanged(object sender, ValueChangedEventArgs e)
        {
            sldBewertungPerson1.Value = GetStepValue(e.NewValue);
        }

        public void OnSliderPerson2ValueChanged(object sender, ValueChangedEventArgs e)
        {
            sldBewertungPerson2.Value = GetStepValue(e.NewValue);
        }

        public double GetStepValue(double value)
        {
            double stepValue = 0.5;
            return Math.Round(value / stepValue) * stepValue;
        }
    }
}