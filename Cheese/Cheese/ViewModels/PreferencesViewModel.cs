﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace Cheese.ViewModels
{
    public class PreferencesViewModel : INotifyPropertyChanged
    {
        #region Properties
        private string imageSource;
        private string user;
        private string accountType;
        private string person1;
        private string person2;
        #endregion

        #region Membervariables
        public string ImageSource
        {
            get { return imageSource; }
            set
            {
                imageSource = value;
                OnPropertyChanged(nameof(ImageSource));
            }
        }
        public string User
        {
            get { return user; }
            set
            {
                user = value;
                OnPropertyChanged(nameof(User));
            }
        }
        public string AccountType
        {
            get { return accountType; }
            set
            {
                accountType = value;
                OnPropertyChanged(nameof(AccountType));
            }
        }
        public string Person1
        {
            get { return person1; }
            set
            {
                person1 = value;
                OnPropertyChanged(nameof(Person1));
            }
        }
        public string Person2
        {
            get { return person2; }
            set
            {
                person2 = value;
                OnPropertyChanged(nameof(Person2));
            }
        }
        #endregion

        #region Membermethods - NotifyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        # region Membermethods - Serialization
        internal static PreferencesViewModel Deserialize(string data)
        {
            return (PreferencesViewModel)JsonConvert.DeserializeObject(data, typeof(PreferencesViewModel));
        }

        internal string Serialize()
        {
            return JsonConvert.SerializeObject(this, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
        }
        #endregion
    }
}
