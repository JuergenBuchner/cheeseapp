﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using Cheese.Models;
using Cheese.Views;

namespace Cheese.ViewModels
{
    public class ItemsViewModel : BaseViewModel
    {
        public ObservableCollection<CheeseRating> AllItems { get; set; }
        public ObservableCollection<CheeseRating> FilteredAndSortedItems { get; set; }
        public Command LoadItemsCommand { get; set; }

        public ItemsViewModel()
        {
            Title = "Bewertungen";
            AllItems = new ObservableCollection<CheeseRating>();
            FilteredAndSortedItems = new ObservableCollection<CheeseRating>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            MessagingCenter.Subscribe<NewItemPage, CheeseRating>(this, "Add", async (obj, item) =>
            {
                var newItem = item as CheeseRating;
                AllItems.Add(newItem);
                await DataStore.AddItemAsync(newItem);
            });
        }

        public async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                await CheeseResource.Instance.LoadResources();

                AllItems.Clear();
                var items = await DataStore.GetItemsAsync(true);
                foreach (var item in items)
                {
                    AllItems.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}