﻿using System;

using Cheese.Models;

namespace Cheese.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public CheeseRating Item { get; set; }
        public ItemDetailViewModel(CheeseRating item = null)
        {
            Title = item?.Name;
            Item = item;
        }
    }
}
