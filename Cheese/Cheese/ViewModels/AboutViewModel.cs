﻿using System;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Cheese.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            Title = "About";
            OpenWebCommand = new Command(async () => await Browser.OpenAsync("https://xamarin.com"));
            AppVersion = VersionTracking.CurrentVersion;
        }
        public string AppVersion { get; set; }

        public ICommand OpenWebCommand { get; }
    }
}