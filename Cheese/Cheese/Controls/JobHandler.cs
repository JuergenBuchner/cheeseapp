﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Cell = DocumentFormat.OpenXml.Spreadsheet.Cell;
using Android.App;
using System.ComponentModel;

namespace Controls
{
    public enum UploadJobType
    {
        WineRating,
        CashEntry,
        WineSuggestion,
        ImprovementProposal,
        EventPhoto,
        HiddenJob,
    }

    #region UploadJob Classes

    #region Base Class
    public abstract class BasicUploadJob : IJobInfo
    {
        public bool jobSucceded = false;

        public void Upload()
        {
            byte[] file = ReceiveFile();
            file = ManipulateFile(file);
            UploadFile(file);
            jobSucceded = true;
        }

        protected abstract byte[] ReceiveFile();

        protected abstract byte[] ManipulateFile(byte[] file);

        protected abstract void UploadFile(byte[] file);


        #region IJobInfo
        public DateTime CreationDate => DateTime.Now;

        public abstract string JobTitle { get; protected set; }

        public abstract UploadJobType JobType { get; }
        public Guid JobId { get; set; }

        public override string ToString()
        {
            return JobTitle;
        }
        #endregion
    }

    public interface IJobInfo
    {
        DateTime CreationDate { get; }
        string JobTitle { get; }
        UploadJobType JobType { get; }
        Guid JobId { get; set; }

    }
    #endregion

    #region Derived Class: Xlsx UploadJob
    public class XlsxUploadJob : BasicUploadJob
    {
        #region Membervariables
        private readonly XlsxPointer initialDataPointer;
        #endregion

        #region Properties
        public IXlsxJobData JobData { get; private set; }
        public XlsxPointer DataPointer { get; private set; }
        #endregion

        #region Constructors
        public XlsxUploadJob(IXlsxJobData jobData, XlsxPointer dataPointer = null)
        {
            this.JobData = jobData;
            JobTitle = jobData.JobTitle;
            initialDataPointer = dataPointer;
        }
        #endregion

        #region Membermethods - Overriden
        protected override byte[] ReceiveFile()
        {
            Task<bool> isFileExisitingTask = Task.Run(() => DropboxUtilities.isFileExisting(JobData.DestinationInfo.Path, JobData.DestinationInfo.Filename));
            isFileExisitingTask.Wait(1000);

            if (!isFileExisitingTask.Result)
            {
                throw new FileNotFoundException("Dropboxfile does not exist!");
            }

            Task<byte[]> downloadTask = Task.Run(() => DropboxUtilities
                    .DownloadByteArray(JobData.DestinationInfo.GetFilepath()));
            downloadTask.Wait(1000);
            return downloadTask.Result;
        }

        protected override byte[] ManipulateFile(byte[] file)
        {
            MemoryStream fileStream = new MemoryStream(); // Dadurch ist er expandable
            fileStream.Write(file, 0, file.Length);
            fileStream.Position = 0;

            SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(fileStream, true);
            WorksheetPart worksheetPart = OpenXmlHelper.GetWorksheetPartByIndex(spreadSheet, JobData.DestinationInfo.WorksheetNumber);
            SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();
            SharedStringTablePart stringTable = spreadSheet.WorkbookPart.SharedStringTablePart;
            Stylesheet stylesheet = spreadSheet.WorkbookPart.WorkbookStylesPart.Stylesheet;

            if (initialDataPointer == null)
                DataPointer = FindListEnd(sheetData, JobData.DestinationInfo.TableStartPoint);
            else
                DataPointer = (XlsxPointer)initialDataPointer.Clone();

            AddTableEntry(ref worksheetPart, ref stringTable, stylesheet, JobData, DataPointer);

            spreadSheet.Save();
            spreadSheet.Close();

            return fileStream.ToArray();
        }


        protected override void UploadFile(byte[] file)
        {
            Task uploadTask = Task.Run(() => DropboxUtilities.UploadFile(JobData.DestinationInfo.GetFilepath(),
                                                                              file));
            uploadTask.Wait();
        }

        #region JobInfo
        public override string JobTitle { get; protected set; }

        public override UploadJobType JobType => JobData.JobType;

        #endregion
        #endregion

        #region Membermethods - Private
        public static XlsxPointer FindListEnd(SheetData sheetData, XlsxPointer tableStartPoint, bool listIsVertical = true)
        {
            if (listIsVertical)
            {
                int maxRowIndex = tableStartPoint.RowIndex + 10000;

                int rowIndex = tableStartPoint.RowIndex;
                for (; rowIndex < maxRowIndex; rowIndex++)
                {
                    Row currentRow = sheetData.Elements<Row>().FirstOrDefault(r => r.RowIndex == rowIndex);
                    if (currentRow == null)
                        return new XlsxPointer(rowIndex, tableStartPoint.ColumnIndex);

                    Cell currentCell = currentRow.Elements<Cell>().FirstOrDefault(c => OpenXmlHelper.GetColumnIndex(c.CellReference.Value) == tableStartPoint.ColumnIndex);

                    if (currentCell == null)
                        return new XlsxPointer(rowIndex, tableStartPoint.ColumnIndex);

                    string entry = currentCell.CellValue?.Text;

                    if (string.IsNullOrEmpty(entry))
                        return new XlsxPointer(rowIndex, tableStartPoint.ColumnIndex);
                }
            }
            else
            {
                int maxColumnIndex = tableStartPoint.ColumnIndex + 10000;

                int columnIndex = tableStartPoint.ColumnIndex;
                for (; columnIndex < maxColumnIndex; columnIndex++)
                {
                    if (columnIndex >= sheetData.Elements<Columns>().Count())
                    {
                        return new XlsxPointer(tableStartPoint.RowIndex, columnIndex);
                    }

                    string entry = sheetData.Elements<Columns>().ToArray()[columnIndex].Elements<Cell>().ToArray()[tableStartPoint.RowIndex].CellValue.Text;

                    if (entry == string.Empty)
                    {
                        return new XlsxPointer(tableStartPoint.RowIndex, columnIndex);
                    }
                }
            }

            throw new IndexOutOfRangeException("EntryPointer for XlsxJob could not be found");
        }

        private void AddTableEntry(ref WorksheetPart worksheetPart, 
                                   ref SharedStringTablePart sharedStringTablePart, 
                                   Stylesheet stylesheet, 
                                   IXlsxJobData jobData, XlsxPointer excelPointer)
        {
            string[] data = jobData.GetData();
            CellValues[] columnFormats = jobData.ColumnFormats;
            SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();

            // Add Row
            if (excelPointer.RowIndex > sheetData.Elements<Row>().Count())
                sheetData.AppendChild<Row>(new Row() { RowIndex = (uint)excelPointer.RowIndex });

            // Add Table Row (Resize Table)
            Table table = worksheetPart.TableDefinitionParts.First().Table;
            string newTableSize = OpenXmlHelper.AddRowToTableSize(table.Reference.Value);
            table.Reference = newTableSize;

            // Add Entry
            // - Get Previous Row/Cells
            Row previousRow;
            if (excelPointer.RowIndex >= 1)
                previousRow = sheetData.Elements<Row>().FirstOrDefault(row => row.RowIndex == excelPointer.RowIndex - 1);
            else
                previousRow = sheetData.Elements<Row>().FirstOrDefault(row => row.RowIndex == 1);
            var previousCells = previousRow.Elements<Cell>();

            // - Insert Data
            var currentRow = sheetData.Elements<Row>().FirstOrDefault(row => row.RowIndex == excelPointer.RowIndex);

            for (int columnOffset = 0; columnOffset < data.Length; columnOffset++)
            {
                var cells = currentRow.Elements<Cell>();

                var previousCell = previousCells.FirstOrDefault(c => c.CellReference.Value == 
                                                                OpenXmlHelper.GetReference(excelPointer.ColumnIndex + columnOffset, excelPointer.RowIndex - 1));


                if (!OpenXmlHelper.CellExistsAtPosition(cells, OpenXmlHelper.GetReference(excelPointer.ColumnIndex + columnOffset, excelPointer.RowIndex)))
                {
                    var clonedCell = new Cell();
                    if (previousCell != null)
                        clonedCell = previousCell.CloneNode(true) as Cell;
                    clonedCell.CellReference = OpenXmlHelper.GetReference(excelPointer.ColumnIndex + columnOffset, excelPointer.RowIndex);
                    clonedCell.CellValue = new CellValue();
                    currentRow.InsertAt(clonedCell, excelPointer.ColumnIndex + columnOffset - 1); // zero-based index required
                }

                var currentCell = currentRow.Elements<Cell>().FirstOrDefault(c => c.CellReference.Value ==
                                                                OpenXmlHelper.GetReference(excelPointer.ColumnIndex + columnOffset, excelPointer.RowIndex));

                string value = data[columnOffset];

                switch (columnFormats[columnOffset])
                {
                    case CellValues.SharedString:
                        currentCell.DataType = new EnumValue<CellValues>(columnFormats[columnOffset]);
                        value = OpenXmlHelper.InsertSharedStringItem(data[columnOffset], sharedStringTablePart).ToString();
                        break;
                    case CellValues.Date:
                        currentCell.DataType = new EnumValue<CellValues>(CellValues.Number); // Funktioniert in Verbindung mit Datum als Formatierung. Datum als Datentyp hat nicht geklappt
                        value = data[columnOffset];
                        break;
                    default:
                        currentCell.DataType = new EnumValue<CellValues>(columnFormats[columnOffset]);
                        value = data[columnOffset];
                        break;
                }            
                

                currentCell.StyleIndex = previousCell?.StyleIndex;

                currentCell.CellValue = new CellValue(value);
            }
        }
        #endregion
    }


    public static class OpenXmlHelper
    {
        public static WorksheetPart GetWorksheetPartByIndex(SpreadsheetDocument ss, int index)
        {
            string Id = ss.WorkbookPart.Workbook.Sheets.Elements<Sheet>().ToArray()[index].Id;
            return (ss.WorkbookPart.GetPartById(Id) as WorksheetPart);
        }
        public static bool CellExistsAtPosition(IEnumerable<Cell> cells, string position)
        {
            return cells.FirstOrDefault(c => c.CellReference.Value == position) != null;
        }

        /// <summary> GetReference allows to convert a index position to an Excel Position. (1,1) => "A1"
        /// <paramref name="row"/> remains its number
        /// <paramref name="column"/> Is one-based and converts to a letter. 1 => A, 2 => B, ...
        /// </summary>
        public static string GetReference(int column, int row)
        {
            return GetColumnName(column) + row.ToString();
        }
        public static string GetColumnName(int colIndex)
        {
            int div = colIndex;
            string colLetter = String.Empty;
            int mod = 0;

            while (div > 0)
            {
                mod = (div - 1) % 26;
                colLetter = (char)(65 + mod) + colLetter;
                div = (int)((div - mod) / 26);
            }
            return colLetter;
        }

        public static string GetColumnName(string cellReference)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);

            return match.Value;
        }

        /// <summary>
        ///  Gets the rowIndex from an Excel CellReference. "A5" => 5, "B1" => 1
        /// </summary>
        /// <param name="cellReference"></param>
        /// <returns></returns>
        public static int GetRowIndex(string cellReference)
        {
            Regex regex = new Regex(@"\d+");
            Match match = regex.Match(cellReference);

            return Int32.Parse(match.Value);
        }

        /// <summary>
        ///  Gets the columnIndex from an Excel CellReference. "A5" => 1, "B1" => 2
        /// </summary>
        /// <param name="cellReference"></param>
        /// <returns></returns>
        public static int GetColumnIndex(string cellReference)
        {
            if (string.IsNullOrEmpty(cellReference))
            {
                return int.MinValue;
            }

            //remove digits
            string columnReference = Regex.Replace(cellReference.ToUpper(), @"[\d]", string.Empty);

            int columnNumber = -1;
            int mulitplier = 1;

            //working from the end of the letters take the ASCII code less 64 (so A = 1, B =2...etc)
            //then multiply that number by our multiplier (which starts at 1)
            //multiply our multiplier by 26 as there are 26 letters
            foreach (char c in columnReference.ToCharArray().Reverse())
            {
                columnNumber += mulitplier * ((int)c - 64);

                mulitplier = mulitplier * 26;
            }

            //the result is zero based so return columnnumber + 1 for a 1 based answer
            //this will match Excel's COLUMN function
            return columnNumber + 1;
        }

        public static SharedStringItem GetSharedStringItemById(WorkbookPart workbookPart, int id)
        {
            return workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(id);
        }

        public static int InsertSharedStringItem(string text, SharedStringTablePart shareStringPart)
        {
            // If the part does not contain a SharedStringTable, create one.
            if (shareStringPart.SharedStringTable == null)
            {
                shareStringPart.SharedStringTable = new SharedStringTable();
            }

            int i = 0;

            // Iterate through all the items in the SharedStringTable. If the text already exists, return its index.
            foreach (SharedStringItem item in shareStringPart.SharedStringTable.Elements<SharedStringItem>())
            {
                if (item.InnerText == text)
                {
                    return i;
                }

                i++;
            }

            // The text does not exist in the part. Create the SharedStringItem and return its index.
            shareStringPart.SharedStringTable.AppendChild(new SharedStringItem(new DocumentFormat.OpenXml.Spreadsheet.Text(text)));
            shareStringPart.SharedStringTable.Save();

            return i;
        }

        internal static string AddRowToTableSize(string tableSize)
        {
            string upperLeft = tableSize.Split(':')[0];
            string lowerRight = tableSize.Split(':')[1];
            int rowIndex = GetRowIndex(lowerRight);

            lowerRight = lowerRight.Replace(rowIndex.ToString(), (rowIndex + 1).ToString());

            return upperLeft + ':' + lowerRight;
        }

        private static Row CopyToLine(Row refRow, uint rowIndex, SheetData sheetData)
        {
            uint newRowIndex;
            var newRow = (Row)refRow.CloneNode(true);
            // Loop through all the rows in the worksheet with higher row 
            // index values than the one you just added. For each one,
            // increment the existing row index.
            IEnumerable<Row> rows = sheetData.Descendants<Row>().Where(r => r.RowIndex.Value >= rowIndex);
            foreach (Row row in rows)
            {
                newRowIndex = System.Convert.ToUInt32(row.RowIndex.Value + 1);

                foreach (Cell cell in row.Elements<Cell>())
                {
                    // Update the references for reserved cells.
                    string cellReference = cell.CellReference.Value;
                    cell.CellReference = new StringValue(cellReference.Replace(row.RowIndex.Value.ToString(), newRowIndex.ToString()));
                }
                // Update the row index.
                row.RowIndex = new UInt32Value(newRowIndex);
            }

            sheetData.InsertBefore(newRow, refRow);
            return newRow;
        }

        internal static string GetCellValue(SpreadsheetDocument spreadsheet, string sheetName, Cell cell)
        {
            return GetCellValue(spreadsheet, sheetName, cell.CellReference);
        }
        internal static string GetCellValue(SpreadsheetDocument spreadsheet, string sheetName, string addressName)
        {
            string value = null;
            // Retrieve a reference to the workbook part.
            WorkbookPart wbPart = spreadsheet.WorkbookPart;

            // Find the sheet with the supplied name, and then use that 
            // Sheet object to retrieve a reference to the first worksheet.
            Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().
                Where(s => s.Name == sheetName).FirstOrDefault();

            // Throw an exception if there is no sheet.
            if (theSheet == null)
            {
                throw new ArgumentException("sheetName");
            }

            // Retrieve a reference to the worksheet part.
            WorksheetPart wsPart =
                (WorksheetPart)(wbPart.GetPartById(theSheet.Id));

            // Use its Worksheet property to get a reference to the cell 
            // whose address matches the address you supplied.
            Cell theCell = wsPart.Worksheet.Descendants<Cell>().
                Where(c => c.CellReference == addressName).FirstOrDefault();

            // If the cell does not exist, return an empty string.
            if (theCell.InnerText.Length > 0)
            {
                value = theCell.InnerText;

                // If the cell represents an integer number, you are done. 
                // For dates, this code returns the serialized value that 
                // represents the date. The code handles strings and 
                // Booleans individually. For shared strings, the code 
                // looks up the corresponding value in the shared string 
                // table. For Booleans, the code converts the value into 
                // the words TRUE or FALSE.
                if (theCell.DataType != null)
                {
                    switch (theCell.DataType.Value)
                    {
                        case CellValues.SharedString:

                            // For shared strings, look up the value in the
                            // shared strings table.
                            var stringTable =
                                wbPart.GetPartsOfType<SharedStringTablePart>()
                                .FirstOrDefault();

                            // If the shared string table is missing, something 
                            // is wrong. Return the index that is in
                            // the cell. Otherwise, look up the correct text in 
                            // the table.
                            if (stringTable != null)
                            {
                                value =
                                    stringTable.SharedStringTable
                                    .ElementAt(int.Parse(value)).InnerText;
                            }
                            break;

                        case CellValues.Boolean:
                            switch (value)
                            {
                                case "0":
                                    value = "FALSE";
                                    break;
                                default:
                                    value = "TRUE";
                                    break;
                            }
                            break;
                    }
                }
            }
            return value;
        }
            
    }

    public interface IXlsxJobData
    {
        XlsxTableStartPoint DestinationInfo { get; }
        string[] GetData();
        string JobTitle { get; }
        UploadJobType JobType { get; }
        CellValues[] ColumnFormats { get; }
    }

    #region Utilities
    public class XlsxTableStartPoint
    {
        #region Properties
        public string Path { get; private set; }
        public string Filename { get; private set; }
        public int WorksheetNumber { get; private set; }
        public XlsxPointer TableStartPoint { get; private set; } // ToDo: Refactor. Gibt es einen allgemeinen "Pointer", welcher docx, txt, xlsx, csv, xml,... unterstützt
        #endregion

        #region Constructors
        public XlsxTableStartPoint(string path, string filename, int worksheetNumber, XlsxPointer tableStartPoint)
        {
            Path = path;
            Filename = filename;
            WorksheetNumber = worksheetNumber;
            TableStartPoint = tableStartPoint;
        }
        #endregion

        #region Membermethods - Public
        public string GetFilepath()
        {
            return Path + '/' + Filename;
        }
        #endregion
    }

    public class XlsxPointer : ICloneable
    {
        #region Properties
        public int RowIndex { get; private set; }
        public int ColumnIndex { get; private set; }
        public int SheetIndex { get; private set; }
        #endregion

        #region Constructors
        public XlsxPointer(int rowIndex, int columnIndex, int sheetIndex = 0)
        {
            RowIndex = rowIndex;
            ColumnIndex = columnIndex;
            SheetIndex = sheetIndex;
        }
        #endregion

        #region Membermethods - Public
        public object Clone()
        {
            return new XlsxPointer(RowIndex, ColumnIndex);
        }
        #endregion
    }
    #endregion
    #endregion

    #region Derived Class: JPEG Job
    public class JPEGJob : BasicUploadJob
    {
        #region Properties
        public IJpegJobData JobData { get; private set; }
        #endregion

        #region Constructors
        public JPEGJob(IJpegJobData jobData)
        {
            JobData = jobData;
            JobTitle = jobData.JobTitle;
        }
        #endregion

        #region Membermethods - Overriden
        protected override byte[] ReceiveFile()
        {
            return JobData.GetData();
        }
        protected override byte[] ManipulateFile(byte[] file)
        {
            return file;
        }
        protected override void UploadFile(byte[] file)
        {
            Task uploadTask = Task.Run(() => DropboxUtilities.UploadFile(JobData.Path + JobData.Filename + ".jpg", file));

            uploadTask.Wait();
        }

        #region IJobInfo
        public override string JobTitle { get; protected set; }

        public override UploadJobType JobType => JobData.JobType;
        #endregion
        #endregion
    }

    public interface IJpegJobData
    {
        string Path { get; }
        string Filename { get; }
        byte[] GetData();
        string JobTitle { get; }
        UploadJobType JobType { get; }
    }
    #endregion

    #endregion UploadJob Classes

    #region DownloadJob Class

    public static class DownloadHandler
    {
        private static async Task<byte[]> DownloadFile(FileInfo fileInfo)
        {
            bool isFileExisitingTask = await DropboxUtilities.isFileExisting(fileInfo.DirectoryName, fileInfo.Name);

            if (!isFileExisitingTask)
            {
                throw new FileNotFoundException("Dropboxfile does not exist!");
            }

            byte[] downloadTask = await DropboxUtilities
                    .DownloadByteArray(fileInfo.FullName);

            return downloadTask;
        }

        public static class Jpeg
        {
            public static async Task<byte[]> DownloadJpegFile(FileInfo fileInfo)
            {
                return await DownloadFile(fileInfo);
            }

            public static async Task<List<byte[]>> DownloadJpegFilesFromFolder(string path)
            {
                List<Task<byte[]>> fileTasks = new List<Task<byte[]>>();

                List<Dropbox.Api.Files.Metadata> list = await DropboxUtilities.GetFolderFileList(path);

                foreach (var data in list)
                {
                    if (System.IO.Path.GetExtension(data.Name).ToLower() == ".jpeg"
                        || System.IO.Path.GetExtension(data.Name).ToLower() == ".jpg")
                    {
                        fileTasks.Add(Task.Run(() => DownloadFile(new FileInfo(data.PathLower))));
                    }
                }
                Task<byte[][]> taskCollection = Task.WhenAll(fileTasks);

                return taskCollection.Result.ToList();
            }
        }

        public static class Xlsx
        {
            public static async Task<SpreadsheetDocument> DownloadXlsxFile(FileInfo fileInfo)
            {
                byte[] file = await DownloadFile(fileInfo);

                MemoryStream fileStream = new MemoryStream(file);
                fileStream.Position = 0;
                return SpreadsheetDocument.Open(fileStream, false);
            }

            public static async Task<List<List<string>>> DownloadXlsxTable(XlsxTableInfo tableInfo)
            {
                SpreadsheetDocument spreadsheetDocument = await DownloadXlsxFile(new FileInfo(tableInfo.Filepath));
                var tableData = GetTableDataFromWorkbook(spreadsheetDocument, tableInfo.SheetIndex, tableInfo.TableName);

                return tableData;
            }

            public class XlsxTableInfo
            {
                public string Filepath;
                public int SheetIndex;
                public string TableName;
                public XlsxTableInfo(string filepath, int sheetIndex, string tableName)
                {
                    Filepath = filepath;
                    SheetIndex = sheetIndex;
                    TableName = tableName;
                }
            }

            private static List<List<string>> GetTableDataFromWorkbook(SpreadsheetDocument spreadsheet, int sheetIndex, string tableName)
            {
                List<List<string>> tableData = new List<List<string>>();
                WorksheetPart worksheetPart = OpenXmlHelper.GetWorksheetPartByIndex(spreadsheet, sheetIndex);
                var sheetName = spreadsheet.WorkbookPart.Workbook.Descendants<Sheet>().ElementAt(sheetIndex).Name;
                SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();

                Table table = worksheetPart.TableDefinitionParts.First(part => part.Table.DisplayName == tableName).Table;

                string upperLeft = table.Reference.Value.Split(':')[0];
                string lowerRight = table.Reference.Value.Split(':')[1];

                int rowStart = OpenXmlHelper.GetRowIndex(upperLeft) + 1; // Header nicht berücksichtigen
                int rowEnd = OpenXmlHelper.GetRowIndex(lowerRight);
                int columnStart = OpenXmlHelper.GetColumnIndex(upperLeft);
                int columnEnd = OpenXmlHelper.GetColumnIndex(lowerRight);

                foreach (Row row in sheetData.Descendants<Row>())
                {
                    if (row.RowIndex > rowEnd)
                        break;

                    if(row.RowIndex >= rowStart)
                    {
                        List<string> rowValues = new List<string>();
                        IEnumerable<Cell> cells = row.Descendants<Cell>();

                        foreach (int columnIndex in Enumerable.Range(columnStart, columnEnd - columnStart +1))//  Cell cell in row.Descendants<Cell>()) 
                        {
                            Cell cell = cells.FirstOrDefault(c => c.CellReference == OpenXmlHelper.GetColumnName(columnIndex) + row.RowIndex.ToString());

                            if (cell != null)
                                rowValues.Add(OpenXmlHelper.GetCellValue(spreadsheet, sheetName, cell));
                            else
                                rowValues.Add(string.Empty);
                        }

                        if (rowValues.Count == columnEnd - columnStart + 1) // Länge ist korrekt
                            tableData.Add(rowValues);
                    }
                }

                return tableData;
            }
        }
    }

    #endregion DownloadJob Class

    #region JobHandler
    public sealed class UploadHandler : Activity, INotifyPropertyChanged
    {
        #region Design-Pattern
        // Design-Pattern: Singleton
        // https://stackoverflow.com/questions/6320393/how-to-create-a-class-which-can-only-have-a-single-instance-in-c-sharp
        private static readonly UploadHandler instance = new UploadHandler();
        public static UploadHandler Instance
        {
            get { return instance; }
        }
        #endregion Design-Pattern

        #region Properties - INotifyPropertyChanged für UI Code
        public event PropertyChangedEventHandler PropertyChanged;
        private bool jobHandlerIsBusy;
        public bool IsBusy
        {
            get
            {
                return jobHandlerIsBusy;
            }
            set
            {
                jobHandlerIsBusy = value;
                NotifyPropertyChanged("JobHandlerIsBusy");
            }
        }
        private void NotifyPropertyChanged(string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region  Properties - Private
        public ObservableCollection<BasicUploadJob> ActiveJobs { get; private set; }
        public ObservableQueue<BasicUploadJob> JobQueue { get; private set; }
        public ObservableCollection<BasicUploadJob> JobHistory { get; private set; }
        #endregion

        #region Constructor
        UploadHandler()
        {
            JobQueue = new ObservableQueue<BasicUploadJob>();
            ActiveJobs = new ObservableCollection<BasicUploadJob>();
            JobHistory = new ObservableCollection<BasicUploadJob>();

            var handler = Task.Run(() => JobHandler());
        }
        #endregion Constructor

        #region JobHandling
        private Task JobHandler()
        {
            while (true)
            {
                if (JobQueue.Count > 0)
                {
                    //System.Threading.Thread.Sleep(10000); Zum Testen
                    this.RunOnUiThread(() =>  // Muss im UiThread laufen, damit man im UI Code (z.B.: Page123.xaml.cs) darauf subscriben kann.
                    {
                        IsBusy = true;
                    });

                    try
                    { 
                        BasicUploadJob job = null;
                        job = JobQueue.Dequeue();

                        if (job == null)
                            continue;

                        ActiveJobs.Add(job);

                        ActiveJobs[0].Upload();

                        AddJobToHistory(ActiveJobs[0]);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.Message);
                        // ToDo: Unterschiedliche Exceptions führen zu Job neu generieren + EnqueueJob oder verwerfen des Jobs
                        // ToDo: Logging speichert die Probleme

                        if (ActiveJobs.Count > 0)
                            JobQueue.Enqueue(ActiveJobs[0]);
                    }
                    finally
                    {
                        ActiveJobs.Clear();

                        this.RunOnUiThread(() => // Muss im UiThread laufen, damit man im UI Code (z.B.: Page123.xaml.cs) darauf subscriben kann.
                        {
                            if(JobQueue.Count <= 0)
                                IsBusy = false;
                        });
                    }
                }
                else
                {
                    System.Threading.Thread.Sleep(1000);
                }
            }
        }
        #endregion JobHandling

        #region MemberMethods - Public
        public void EnqueueJob(BasicUploadJob job)
        {
            job.JobId = Guid.NewGuid();
            JobQueue.Enqueue(job);
        }
        #endregion

        #region MemberMethods - Private
        private void AddJobToHistory(BasicUploadJob job)
        {
            JobHistory.Add(job);
            // ToDo: Event feuern, dass neuer Job hinzugefügt wurde
            if (JobHistory.Count > 100)
            {
                JobHistory.RemoveAt(0);
            }
        }
        #endregion
    }

    /*
        public static class DownloadHandler
        {
            #region MemberMethods
            public async Task<T> Download(IDownloadJob job)
            {
                job.Download
                return 1;
            }
            #endregion MemberMethods
        }*/

    public class ObservableQueue<T> : ObservableCollection<T>, INotifyCollectionChanged
    {
        public void Enqueue(T obj)
        {
            base.Add(obj);
        }

        public T Dequeue()
        {
            T obj = base.Items[0];
            base.Remove(obj);
            return obj;
        }
    }

    #endregion JobHandler
}
