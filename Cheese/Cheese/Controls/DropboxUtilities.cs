﻿using Dropbox.Api;
using Dropbox.Api.Files;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Controls
{
    public static class DropboxUtilities
    {
        #region Authentification
        static string dropboxAppKey = "hg4wspo59o0bv73";
        static string dropboxAppSecret = "swmebxf4y5af9q7";
        //static string RedirectUri = "http://localhost";
        static string RedirectUri = "https://www.google.com/";
        static string oauth2State = "cheeseState_joqij34fj935";
        static string _AccessToken = null;
        private static DropboxClient _dbxClient = null;

        public static event PropertyChangedEventHandler AccessTokenChanged;
        public static string AccessToken 
        { 
            get
            {
                if (_AccessToken == null)
                {
                    if(Application.Current.Properties.ContainsKey("AccessToken"))
                        _AccessToken = Application.Current.Properties["AccessToken"] as string;
                }

                return _AccessToken;
            }
            set
            {
                Application.Current.Properties["AccessToken"] = value;
                _AccessToken = value;
                OnPropertyChanged();
            }
        }

        public static void OnPropertyChanged([CallerMemberName] string name = null)
        {
            AccessTokenChanged.Invoke(name, new PropertyChangedEventArgs(name));
        }

        public static DropboxClient DropboxClient
        {
            get
            {
                if (_dbxClient == null)
                {
                    if (AccessToken == null)
                    {
                        Task auth = Task.Run(() => Authorize());
                        auth.Wait();
                    }
                    if (AccessToken == null)
                        throw new Exception("Die Anmeldung zu Dropbox war nicht erfolgreich. Der Dropbox Client kann nicht genutzt werden.");
                    _dbxClient = GetClient(AccessToken);
                }
                return _dbxClient;
            }
            set
            {

            }
        }

        public static async Task Authorize()
        {
            if (string.IsNullOrWhiteSpace(AccessToken) == false)
            {
                // Already authorized
                return;
            }

            // Run Dropbox authentication
            var authorizeUri = Dropbox.Api.DropboxOAuth2Helper.GetAuthorizeUri(Dropbox.Api.OAuthResponseType.Token, dropboxAppKey, new Uri(RedirectUri), oauth2State);
            var webView = new WebView { Source = new UrlWebViewSource { Url = authorizeUri.AbsoluteUri } };
            webView.Navigating += WebViewOnNavigating;
            var contentPage = new ContentPage { Content = webView };
            await Application.Current.MainPage.Navigation.PushModalAsync(contentPage);
        }


        private static async void WebViewOnNavigating(object sender, WebNavigatingEventArgs e)
        {
            if (!e.Url.StartsWith(RedirectUri, StringComparison.OrdinalIgnoreCase))
            {
                // we need to ignore all navigation that isn't to the redirect uri.
                return;
            }

            try
            {
                var result = Dropbox.Api.DropboxOAuth2Helper.ParseTokenFragment(new Uri(e.Url));

                if (result.State != oauth2State)
                {
                    return;
                }

                AccessToken = result.AccessToken;
            }
            catch (ArgumentException)
            {
                // There was an error in the URI passed to ParseTokenFragment
            }
            finally
            {
                e.Cancel = true;
                await Application.Current.MainPage.Navigation.PopModalAsync();
            }
        }
        #endregion


        private static DropboxClient GetClient(string oauth2AccessToken)
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                var config = new DropboxClientConfig();
                config.HttpClient = new HttpClient(new HttpClientHandler());
                config.HttpClient.Timeout = new TimeSpan(0, 0, 30);

                return new DropboxClient(oauth2AccessToken, config);

                //return new DropboxClient(oauth2AccessToken, new DropboxClientConfig()
                //{
                //    HttpClient = new HttpClient(new
                //       HttpClientHandler())
                //});
            }
            return new DropboxClient(oauth2AccessToken);
        }

        public static async Task<List<Metadata>> GetFolderFileList(string directory)
        {
            ListFolderResult result = await DropboxClient.Files.ListFolderAsync(directory, false, false, false, false, false);

            return result.Entries.ToList();
        }

        public static async Task<bool> isFileExisting(string path)
        {
            string folder = Path.GetDirectoryName(path);
            string filename = Path.GetFileName(path);

            return await isFileExisting(folder, filename);
        }

        public static async Task<bool> isFileExisting(string folder, string filename)
        {
            bool isExisting = false;
            try
            {
                List<Metadata> list = await GetFolderFileList(folder);

                foreach (var item in list.Where(i => i.IsFile))
                {
                    if (item.Name.ToLower() == filename.ToLower()) // case-insensitive
                    {
                        isExisting = true;
                        break;
                    }
                }
            }
            catch (DropboxException e)
            {
                // Request did not work - maybe AccessToken wrong
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.Message);
            }

            return isExisting;
        }

        public static async Task<byte[]> DownloadByteArray(string filepath)
        {
            try
            {
                using (var response = await DropboxClient.Files.DownloadAsync(filepath))
                {
                    return await response.GetContentAsByteArrayAsync();
                }
            }
            catch (DropboxException e)
            {
                // Drobox HTTP Request did not work - maybe AccessToken wrong
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.Message);
                throw new FileLoadException("Drobox HTTP Request did not work - maybe AccessToken wrong");
            }

        }
        public static async Task UploadFile(string filepath, byte[] file)
        {
            Stream stream = new MemoryStream(file);
            await UploadFile(filepath, stream);
        }
        public static async Task UploadFile(string filepath, Stream stream)
        {
            try
            {
                var updated = await DropboxClient.Files.UploadAsync(
                filepath,
                WriteMode.Overwrite.Instance,
                body: stream);
            }
            catch (DropboxException e)
            {
                // Drobox HTTP Request did not work - maybe AccessToken wrong
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.Message);
                throw new FileLoadException("Drobox HTTP Request did not work - maybe AccessToken wrong");
            }
        }

        /*
        public static List<Metadata> GetFileMetadataFromFolder(string path = "")
        {
            List<Metadata> fileList = new List<Metadata>();

            List<ListFolderResult> listTask = await dbx.Files.ListFolderAsync(path);

            listTask.Wait(5000);

            IList<Metadata> test = listTask.Result.Entries;

            return listTask.Result.Entries.Where(i => i.IsFile).ToList();
        }
        
        public static async Task ListRootFolder(string path = "")
        {
            var list = await dbx.Files.ListFolderAsync(path);

            // show folders then files
            foreach (var item in list.Entries.Where(i => i.IsFolder))
            {
                Console.WriteLine("D  {0}/", item.Name);
            }

            foreach (var item in list.Entries.Where(i => i.IsFile))
            {
                Console.WriteLine("F{0,8} {1}", item.AsFile.Size, item.Name);
            }
        }
        public static async Task Download(string folder, string file)
        {
            using (var response = await dbx.Files.DownloadAsync(folder + "/" + file))
            {
                Console.WriteLine(await response.GetContentAsStringAsync());
            }
        }

        public static async Task UploadStream(string folder, string file, Stream content)
        {
            var updated = await dbx.Files.UploadAsync(
                folder + "/" + file,
                WriteMode.Overwrite.Instance,
                body: content);
            Console.WriteLine("Saved {0}/{1} rev {2}", folder, file, updated.Rev);
        }

        public static async Task Upload(string folder, string file, string content)
        {
            using (var mem = new MemoryStream(Encoding.UTF8.GetBytes(content)))
            {
                await UploadStream(folder, file, mem);
            }
        }*/
    }

}
