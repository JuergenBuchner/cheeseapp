﻿using System;
using System.Collections.Generic;
using System.Text;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using System.IO;
using System.Reflection;
using Controls;
using System.Threading.Tasks;
using Cheese.Models;

namespace Cheese.Controls
{
    public static class InitialStartup
    {
        public static async Task Initialize()
        {
            // Login

            // Create Folders

            // Create Files
            string[] resourceNames = Assembly.GetExecutingAssembly().GetManifestResourceNames();
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Cheese.FileResources.CheeseTable_Template.xlsx");

            Task<bool> isFileExisitingTask = Task.Run(() => DropboxUtilities.isFileExisting(new CheeseRating().DestinationInfo.GetFilepath()).Result);
            isFileExisitingTask.Wait(1000);

            if (!isFileExisitingTask.Result)
                await DropboxUtilities.UploadFile(new CheeseRating().DestinationInfo.GetFilepath(), stream);
        }
    }
}
