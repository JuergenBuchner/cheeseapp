﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Cheese.Services;
using Cheese.Views;
using Controls;

namespace Cheese
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new MainPage();
        }
        private event EventHandler Starting = delegate { };

        //Application developers override this method  
        //to perform actions when the application starts.
        protected override void OnStart()
        {
            //subscribe to event
            Starting += onStarting;
            //raise event
            Starting(this, EventArgs.Empty);
        }

        private async void onStarting(object sender, EventArgs args)
        {
            //unsubscribe from event
            Starting -= onStarting;

            //if (DropboxUtilities.AccessToken == null)
            //    await DropboxUtilities.Authorize();

            //CheeseResource.Instance.LoadResources();
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
