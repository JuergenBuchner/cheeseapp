﻿using Cheese.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controls;

namespace Cheese
{
    public sealed class CheeseResource
    {
        #region Design-Pattern
        // Design-Pattern: Singleton
        // https://stackoverflow.com/questions/6320393/how-to-create-a-class-which-can-only-have-a-single-instance-in-c-sharp
        private static readonly CheeseResource instance = new CheeseResource();
        public static CheeseResource Instance
        {
            get { return instance; }
        }
        #endregion Design-Pattern

        #region Constructor
        private CheeseResource()
        {
            InitializeDefaultResources();
        }
        #endregion

        #region Resource Management
        public void InitializeDefaultResources()
        {
            CheeseRatingCollection = new ObservableCollection<CheeseRating>();
        }
        #endregion

        #region Static Resources
        public static List<string> CheeseTypes => new List<string>() {"Hartkäse",
                                                                "Schnittkäse",
                                                                "Halbfester Schnittkäse",
                                                                "Sauermilchkäse",
                                                                "Weichkäse",
                                                                "Schnittkäse"};
        #endregion

        #region Dynamic Resources
        public ObservableCollection<CheeseRating> CheeseRatingCollection { get; set; }
        #endregion

        public async Task LoadResources() 
        {
            List<List<string>> table = await DownloadHandler.Xlsx.DownloadXlsxTable(new DownloadHandler.Xlsx.XlsxTableInfo(CheeseResources.CheeseRatingsFolder + "/" + CheeseResources.CheeseRatingFile, 0, "Tabelle1"));
            List<CheeseRating> cheeseRatings = GetRowsFromTable(table);
            CheeseRatingCollection.Clear();
            cheeseRatings.ForEach(entry => CheeseRatingCollection.Add(entry));
        }

        public List<CheeseRating> GetRowsFromTable(List<List<string>> table)
        {
            List<CheeseRating> cheeseRatings = new List<CheeseRating>();
            for (int i = 0; i < table.Count(); i++)
            {
                List<string> row = table[i];
                string[] dataRow = row.ToArray();

                if (dataRow[0] == string.Empty)
                    break;
                try
                {
                    cheeseRatings.Add(CheeseRating.ParseFromRawData(dataRow));
                }
                catch
                {
                    // ToDo: Logging: Käsebewertung konnte nicht geparsed werden
                }
            }

            return cheeseRatings;
        }
    }
}
