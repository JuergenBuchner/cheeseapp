﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Wine.Utilities
{
    public static class UIUtilities
    {
        #region Exception Handling
        public static void ThrowExceptionIfEntryIsNullOrEmpty(Entry textEntry, string entryName = "")
        {
            if (String.IsNullOrEmpty(textEntry.Text))
                throw new ArgumentException(String.Format("Das Eingabefeld '{0}' wurde nicht ausgefüllt!", textEntry.Placeholder??entryName));
        }

        public static void ThrowExceptionIfEditorIsNullOrEmpty(Editor editorEntry, string entryName = "")
        {
            if (String.IsNullOrEmpty(editorEntry.Text))
                throw new ArgumentException(String.Format("Das Eingabefeld '{0}' wurde nicht ausgefüllt!", editorEntry.Placeholder??entryName));
        }

        public static void ThrowExceptionIfPickerIsUnselected(Picker picker, string entryName = "")
        {
            if (picker.SelectedItem == null)
            {
                throw new ArgumentException(String.Format("Es wurde für '{0}' keine Auswahl getroffen!", picker.Title??entryName));
            }
        }

        public static void ThrowExceptionIfEntryIsNoInteger(Entry editorEntry, string entryName = "")
        {
            if (!Int64.TryParse(editorEntry.Text, out Int64 result))
                throw new ArgumentException(String.Format("Das Eingabefeld '{0}' enthält keine natürliche Zahl!", editorEntry.Placeholder ?? entryName));
        }
        #endregion

    }
}
