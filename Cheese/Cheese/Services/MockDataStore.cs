﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Cheese.Models;

namespace Cheese.Services
{
    public class MockDataStore : IDataStore<CheeseRating>
    {
        readonly List<CheeseRating> items;

        public MockDataStore()
        {
            items = new List<CheeseRating>()
            {
                //new CheeseRating { Id = Guid.NewGuid().ToString(), Name = "First item", Kommentar="This is an item description." },
                //new CheeseRating { Id = Guid.NewGuid().ToString(), Name = "Second item", Kommentar="This is an item description." },
                //new CheeseRating { Id = Guid.NewGuid().ToString(), Name = "Third item", Kommentar="This is an item description." },
                //new CheeseRating { Id = Guid.NewGuid().ToString(), Name = "Fourth item", Kommentar="This is an item description." },
                //new CheeseRating { Id = Guid.NewGuid().ToString(), Name = "Fifth item", Kommentar="This is an item description." },
                //new CheeseRating { Id = Guid.NewGuid().ToString(), Name = "Sixth item", Kommentar="This is an item description." }
            };

            CheeseResource.Instance.CheeseRatingCollection.CollectionChanged += OnCheeseRatingChanged;
        }

        public async Task<bool> AddItemAsync(CheeseRating item)
        {
            items.Add(item);
            items.Sort();
            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(CheeseRating item)
        {
            var oldItem = items.Where((CheeseRating arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((CheeseRating arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<bool> ClearItemsAsync()
        {
            items.Clear();
            return await Task.FromResult(true);
        }

        public async Task<CheeseRating> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<CheeseRating>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }

        public async void OnCheeseRatingChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if(e.NewItems != null)
            {
                foreach (var item in e.NewItems)
                {
                    await AddItemAsync(item as CheeseRating);
                }
            }

            if(e.OldItems != null)
            {
                foreach (var item in e.OldItems)
                {
                    await DeleteItemAsync((item as CheeseRating).Id);
                }
            }

            if (e.Action == NotifyCollectionChangedAction.Reset)
                await ClearItemsAsync();
        }
    }
}