﻿using DocumentFormat.OpenXml.Spreadsheet;
using System;
using Controls;

namespace Cheese.Models
{
    public static class CheeseResources
    {
#if DEBUG
        public static string CheeseRatingsFolder => "/Hobbies/KäseAppTests/Essen";
#endif
#if RELEASE
        public static string CheeseRatingsFolder => "/Hobbies/Essen";
#endif
        public static string CheeseRatingFile => "Käsebewertungen.xlsx";
    }
    public class CheeseRating : IXlsxJobData, IComparable<CheeseRating>
    {
        public string Id { get; set; }

        public DateTime Datum { get; set; }
        public string Barcode { get; set; }
        public string Erwerb { get; set; }
        public string Name { get; set; }
        public string Hersteller { get; set; }
        public string Sorte { get; set; }
        public string Beschreibung { get; set; }
        public double BewertungPerson1 { get; set; }
        public double BewertungPerson2 { get; set; }
        public double BewertungDurchschnitt
        {
            get
            {
                return (BewertungPerson1 + BewertungPerson2) / 2;
            }
        }
        public double Bewertung { get; set; } // Wird für die Darstellung verwendet. Kann den Wert von Person1, Person2 oder den Durchschnitt enthalten. Durchschnitt ist Default

        public CheeseRating()
        {
            Datum = DateTime.Now;
        }

        public XlsxTableStartPoint DestinationInfo => new XlsxTableStartPoint(CheeseResources.CheeseRatingsFolder, CheeseResources.CheeseRatingFile, 0, new XlsxPointer(2,1));

        public string JobTitle => Name;

        public UploadJobType JobType => UploadJobType.HiddenJob;

        public CellValues[] ColumnFormats => new CellValues[] { CellValues.Date, 
                                                                CellValues.Number, 
                                                                CellValues.SharedString,
                                                                CellValues.SharedString,
                                                                CellValues.SharedString,
                                                                CellValues.SharedString,
                                                                CellValues.SharedString,
                                                                CellValues.Number,
                                                                CellValues.Number};

        public string[] GetData()
        {
            return new string[] { Datum.ToOADate().ToString(), Barcode ?? "", Erwerb ?? "", Name ?? "", Hersteller ?? "", Sorte ?? "", Beschreibung ?? "", BewertungPerson1.ToString(), BewertungPerson2.ToString() };
        }

        public static CheeseRating ParseFromRawData(string[] data)
        {
            CheeseRating item = new CheeseRating();
            item.Datum = DateTime.FromOADate(Double.Parse(data[0]));
            item.Barcode = data[1];
            item.Erwerb = data[2];
            item.Name = data[3];
            item.Hersteller = data[4];
            item.Sorte = data[5];
            item.Beschreibung = data[6];
            item.BewertungPerson1 = Convert.ToDouble(data[7]);
            item.BewertungPerson2 = Convert.ToDouble(data[8]);
            item.Bewertung = item.BewertungDurchschnitt;

            return item;
        }

        public int CompareTo(CheeseRating other)
        {
            double ratingDiff = this.Bewertung - other.Bewertung;

            if (ratingDiff > 0)
                return -1;
            else if (ratingDiff < 0)
                return 1;
            else
                return 0;
        }
    }
}